<div class="idgham">
  <p><strong>Idgham</strong></p>
  <p><span style="font-weight: 400;">Idgham adalah memasukkan satu perkara pada perkara yang lain. </span></p>
  <p><span style="font-weight: 400;">Idgham dibagi menjadi 2 yaitu :</span></p>
  <p>- Idgham Bighunnah</p>
  <p>- Idgham Bilaghunnah</p>
  <p><span style="font-weight: 400;">Idgham Bighunnah dilakukan dengan memasukkan / meleburkan huruf nun mati atau tanwin kedalam huruf sesudahnya dengan disertai dengung, jika bertemu dengan salah satu huruf dari keempat huruf yaitu : ن, م, و, ى</span></p>
  <p><span style="font-weight: 400">Idgham Bilaghunnah dilakukan dengan memasukkan / meleburkan huruf nun mati atau tanwin kedalam huruf sesudahnya tanpa disertai dengung, jika bertemu dengan huruf (ر، ل)</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Contoh dari Hukum Idgham Bighunnah : <img src = 'assets/foto/nun/idgham bighunnah/nun/nun1.png'><img src = 'assets/foto/nun/idgham bighunnah/nun/nun2.png'></span></p>
  <p><span style="font-weight: 400;">Contoh dari Hukum Idgham Bilaghunnah : <img src = 'assets/foto/nun/idgham bilaghunnah/lam1.png'><img src = 'assets/foto/nun/idgham bilaghunnah/lam2.png'></span></p>
  <p></p>
</div>
