<div class="qalqalah">
  <p><strong>Qalqalah</strong></p>
  <p><span style="font-weight: 400;">Menurut bahasa qalqalah artinya gerak, sedangkan menurut istilah qalqalah adalah bunyi huruf yang memantul bila ia mati atau dimatikan, atau suara membalik dengan bunyi rangkap</span></p>
  <p><span style="font-weight: 400;">Huruf qalqalah terdiri atas lima huruf, yaitu : ق , ط , ب , ج , د </span></p>
  <p><span style="font-weight: 400;">Qalqalah terbagi menjadi 2 yaitu:</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Qalqalah Sugra</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Qalqalah Kubra</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Qalqalah kubra (besar) yaitu Huruf Qalqalah yang berbaris hidup, dimatikan karena waqaf. inilah Qalqalah yang paling utama, cara membacanya dikeraskan qalqalahnya.</span></p>
  <p><span style="font-weight: 400;">Dalam keadaan ini, qalqalah dilakukan apabila bacaan diwaqafkan tetapi tidak diqalqalahkan apabila bacaan diteruskan </span></p> 
  <p></p>
  <p></p>
  <p><span style="font-weight: 400;">Qalqalah Sugra (kecil) yaitu Huruf Qalqalah yang berbaris mati, tetapi tidak waqaf padanya,caranya membacanya kurang dikeraskan Qalqalahnya.</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Contoh dari Hukum Qalqalah Sugra :</p>
  <p><img src = 'assets/foto/qalqalah/sugra/sugra1.png'></p>
  <p><img src = 'assets/foto/qalqalah/sugra/sugra2.png'></span></p>
  <p><span style="font-weight: 400;">Contoh dari Hukum Qalqalah Kubra :</p>
  <p><img src = 'assets/foto/qalqalah/kubra/kubra1.png'></p>
  <p><audio controls><source src ="assets/audio/qal.mp3" type="audio/mpeg"></audio></p>
  <p><img src = 'assets/foto/qalqalah/kubra/kubra2.png'></span></p>
  </div>