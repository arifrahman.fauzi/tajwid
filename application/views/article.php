<div class="Article">
  <p><strong>Waqaf</strong></p>
  <p><span style="font-weight: 400;">Waqaf Mutlaq (ط)
Waqaf Mutlaq tandanya ط. Apabila kita membaca Al Qur'an menemui tanda waqaf tersebut, maka lebih utama diwaqafkan atau berhenti pada tanda waqaf tersebut.</span></p>
  <p><span style="font-weight: 400;">Waqaf Lazim (م)
Waqaf Lazim tandanya م. Apabila pada ayat Al Qur'an terdapat tanda waqaf lazim, maka cara membacanya adalah harus berhenti.</span></p>
 <p><span style="font-weight: 400;">Waqaf Jaiz (ج)
Apabila pada ayat Al Qur'an terdapat tanda waqaf jaiz, maka cara membacanya boleh berhenti dan boleh dilanjutkan dengan kata berikutnya.</span></p>
<p><span style="font-weight: 400;">Waqaf Waslu Ula (صلى)
Apabila pada ayat Al Qur'an terdapat tanda waqaf ini, cara membacanya adalah lebih utama dilanjutkan dengan kata berikutnya.</span></p>
<p><span style="font-weight: 400;">Waqaf Mustahab (قيف)
Apabila pada ayat Al Qur'an terdapat tanda waqaf ini, diutamakan berhenti pada kata yang terdapat tanda tersebut.</span></p>
<p><span style="font-weight: 400;">Waqaf Waqfu Ula (قال)
Apabila pada ayat Al Qur'an terdapat tanda waqaf ini, diutamakan berhenti pada kata yang terdapat tanda tersebut.
</span></p>
<p><span style="font-weight: 400;">Waqaf Mujawwaz (ز)
Apabila pada ayat Al Qur'an terdapat tanda waqaf ini, diutamakan terus pada kata yang terdapat tanda tersebut, tetapi boleh juga waqof.</span></p>
<p><span style="font-weight: 400;">Waqaf Murakhas (ص)
Apabila pada ayat Al Qur'an terdapat tanda waqaf ini, boleh berhenti pada kata yang terdapat tanda tersebut karena darurat yang disebabkan oleh panjangnya ayat atau kehabisan nafas , tetapi diutamakan waslah/terus.</span></p>
<p><span style="font-weight: 400;">Waqaf Qobih (ق)
Apabila pada ayat Al Qur'an terdapat tanda waqaf ini, diutamakan terus pada kata yang terdapat tanda tersebut.</span></p>
<p><span style="font-weight: 400;">Waqaf Laa Washal (لا)
Apabila pada ayat Al Qur'an terdapat tanda waqaf ini, jangan waqof kecuali jika di bawahnya terdapat tanda awal ayat yang membolehkan waqof secara mutlaq, maka boleh berhenti tanpa di ulang lagi.</span></p>
<p><span style="font-weight: 400;">Waqaf Mu'anaqah (. ۛ.  . ۛ.)
Apabila pada ayat Al Qur'an terdapat tanda waqaf ini, harus berhenti di salah satu dari kedua kelompok titik tiga tersebut, boleh pada yang pertama atau yang kedua.</span></p>
<p><span style="font-weight: 400;">Waqaf Saktah (ساكته)
Apabila pada ayat Al Qur'an terdapat tanda waqaf ini, harus berhenti dan diam sejenak tanpa mengambil nafas baru pada kata yang terdapat tanda tersebut. Saktah Sakat adalah diam sejenak biar putus dan pisah suaranya dengan tanpa berganti nafas.</span></p>

  </div>