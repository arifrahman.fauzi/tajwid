<!DOCTYPE html>
<html>
  <!--update dari arif-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Hukum Tajwid</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css'); ?>">
  <link rel="icon" href="assets/foto/icon.png">
  <script src="<?php echo base_url('assets/jquery-3.3.1.min.js'); ?>"></script>
  <link rel="stylesheet" href="<?php echo base_url().'assets/web-fonts-with-css/css/fontawesome-all.min.css' ?>">
  <script src="<?php echo base_url().'assets/web-fonts-with-css/svg-with-js/js/fontawesome-all.min.js' ?>" charset="utf-8"></script>
  </head>

<body>
  <div class="navbar-header" >
    <ul class="nav nav-pills" style="background-color:#0f1920">
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
    </ul>
  </div>
  <!-- HEADER -->
  <div class="container ful-wdth">
    <header class="iner img-responsive hero-spacer-st">
      <div class="hed1">
        <h1 class="hed1 py-3 text-center">HUKUM TAJWID </h1>
      </div>
    </header>
    <div class="row">
      <div class="col-lg-12 mg-t bd">
        <ul id="myTab" class="nav nav-pills nav-fill nav-tabs nav-justified">
          <li class="active nav-item"><a id="bs" href="#service-one" data-toggle="tab" aria-expanded="true"><i class="fa fa-home fa-sm"></i> Basic</a> </li>
          <li class="nav-item"><a id="ad" href="#service-two" data-toggle="tab" aria-expanded="false"> Advance</a> </li>
        </ul>

        <div id="myTabContent" class="tab-content py-5">
          <div class="tab-pane fade active in" id="service-one">
            <div class="row mg-t">
              <div class="col-md-3">
                <div class="list-group">
                  <a id="intro" class="list-group-item active " >Pengenalan</a>
				  
                  <a id="idzhar" class="list-group-item" >Idzhar</a>
                  <a id="ikhfa" class="list-group-item " >Ikhfa'</a>
                  <a id="idgham" class="list-group-item " >Idgham</a>
                  <a id="iqlab" class="list-group-item " >Iqlab</a>

                  <a id="ikhfa_syafawi" class="list-group-item " >Ikhfa' Syafawi</a>
                  <a id="idgham_mimmi" class="list-group-item " >Idgham Mimmi</a>
                  <a id="idzhar_syafawi" class="list-group-item " >Idzhar Syafawi</a>
                  <a id="qalqalah" class="list-group-item " >Qalqalah</a>
                  <a id="ra" class="list-group-item " >Ra'</a>
                  <a id="mad" class="list-group-item " >Mad</a>
                  <a id="alif_lam" class="list-group-item " >Alif Lam</a>


                </div>

              </div>
              <div class="col-md-9 bd-1">
                  <div class="row">
                    <h2 id="tajweed-heading">Pengenalan Tajwid</h2>
                    <img class="img-responsive img-rounded mgn lft" src="<?php echo base_url().'assets/foto/11.jpg' ?>"/>
                  </div>
                  <!-- Content Column -->
                  <div class="row tp">
                    <div class="content">
                      <p><strong>Apa itu Tajwid?:</strong></p>
                      <p><span style="font-weight: 400;">Tajwid merupakan kata lain dari melakukan sesuatu dengan elok dan indah.</span></p>
                      <p><span style="font-weight: 400;">Dalam ilmu Qira'ah, tajwid berarti mengeluarkan huruf dari tempatnya dengan memberikan sifat yang dimilikinya</span></p>
                      <p>&nbsp;</p>
                      <p><span style="font-weight: 400;">Allah SWT berfirman :</span></p>
                      <p><strong><em>"Bacalah Al-Qur'an secara perlahan dan baca surat-suratnya dengan jelas."</em></strong></p>
                      <p>&nbsp;</p>
                      <p><strong> Keindahan dari membaca Al-Qur'an</strong></p>
                      <p>&nbsp;</p>
                      <p><span style="font-weight: 400;">Rasulullah SAW bersabda:</span></p>
                      <p><strong><em>"Perindah Al-Qur'an dengan suaramu, sesungguhnya suara yang indah meningkatkan keindahan Al-Qur'an"</em></strong></p>
                    </div>
                  </div>
              </div>
            </div>

          </div>

          <!-- service two -->
          <div class="tab-pane fade" id="service-two">
            <div class="row mg-t">
              <div class="col-md-3">
                <div class="list-group">
					 <a data-toggle="collapse" href="#collapseOne" class="list-group-item">Content</a>
		  <div id="collapseOne" class="panel-collapse collapse in">
		   <ul class="list-group">
				  <li id="artikel"class="list-group-item">Waqaf</li>
				  
            </ul>
          </div>
                  
				 
                </div>

              </div>
			  <div class="col-md-9 bd-1">
                  <div class="row">
                    <h2 id="tajweed-heading">Introduction to Tajweed</h2>
                    <img class="img-responsive img-rounded mgn lft" src="<?php echo base_url().'assets/foto/11.jpg' ?>"/>
                  </div>
                  <!-- Content Column -->
                  <div class="row tp">
                    <div class="content">
                      <p><strong>Advanced Learning</strong></p>
                      <p><span style="font-weight: 400;">Tajweed means to make beauty in reading. </span></p>
                      <p><span style="font-weight: 400;">It means to pronounce every letter correctly with all its qualities </span></p>
                      <p>&nbsp;</p>
                      <p><span style="font-weight: 400;">Almighty Allah Says in the Holy Quran that::</span></p>
                      <p><strong><em>"Recite hte Holy Quran slowly and making the letters clear"</em></strong></p>
                      <p>&nbsp;</p>
                      <p><strong> BEAUTY OF THE HOLY QURAN</strong></p>
                      <p>&nbsp;</p>
                      <p><span style="font-weight: 400;">Saying of the Holy Prophet (P.B.U.H) :</span></p>
                      <p><strong><em>Beautify the Holy Quran with your voices, definitely beautiful voices enhance the beauty of the Holy Quran </em></strong></p>
                    </div>
                  </div>
              </div>

            </div>

          </div>

        </div>
      </div>
    </div>

  </div>

  <!-- fungsi -->
  <script type="text/javascript">
  $(document).ready(function(){
		$('.list-group-item').click(function(){
			var menu = $(this).attr('id');
			if(menu == "idzhar"){
				$('.content').load('welcome/idzhar');
			}else if(menu == "artikel"){
        $('.content').load('welcome/article')
      }else if(menu == "ikhfa"){
				$('.content').load('welcome/ikhfa');
			}else if(menu == "Mad"){
        $('.content').load('welcome/mad');
      }else if(menu == "advanced"){
				$('.content').load('welcome/advanced');
			}else if(menu == "isymam"){
				$('.content').load('welcome/isymam');
			}else if(menu == "idgham"){
				$('.content').load('welcome/idgham');
			}else if(menu == "iqlab"){
        $('.content').load('welcome/iqlab');
      }else if(menu == "ikhfa_syafawi"){
        $('.content').load('welcome/ikhfa_syafawi');
      }else if(menu == "idgham_mimmi"){
        $('.content').load('welcome/idgham_mimmi');
      }else if(menu == "idzhar_syafawi"){
        $('.content').load('welcome/idzhar_syafawi');
      }else if(menu == "qalqalah"){
        $('.content').load('welcome/qalqalah');
      }else if(menu == "ra"){
        $('.content').load('welcome/ra');
      }else if(menu == "mad"){
        $('.content').load('welcome/mad');
      }else if(menu == "alif_lam"){
        $('.content').load('welcome/alif_lam');
      }
			
		});


		// halaman yang di load default pertama kali
		$('.badan').load('home.php');

	});
  </script>


  <!-- Header -->
  <script src="<?php echo base_url('assets/jquery-3.3.1.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>

</body>

</html>
