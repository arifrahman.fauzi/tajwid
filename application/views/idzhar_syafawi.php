<div class="idzhar_syafawi  ">
  <p><strong>Idzhar Syafawi</strong></p>
  <p><span style="font-weight: 400;">Apabila mim mati  (مْ)  bertemu dengan salah satu huruf hijaiyyah selain huruf mim (مْ) dan ba (ب), maka cara membacanya dengan jelas di bibir dan mulut tertutup</span></p>
  <p><span style="font-weight: 400;">Izhar Syafawi dibaca dengan jelas tanpa dengung.</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Contoh dari Hukum Idzhar Syafawi : </span></p>
  <p><img src = 'assets/foto/tajwid mim/idzhar syafawi/idzharSyafawi1.png'></p>
  <p><img src = 'assets/foto/tajwid mim/idzhar syafawi/idzharSyafawi2.png'></p>
</div>