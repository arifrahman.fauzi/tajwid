<div class="iqlab">
  <p><strong>Iqlab</strong></p>
  <p><span style="font-weight: 400;">Iqlab artinya adalah menukar atau mengganti</span></p>
  <p><span style="font-weight: 400;">Apabila ada nun mati atau tanwin(ـًـٍـٌ / نْ) bertemu dengan huruf ba (ب), maka cara membacanya dengan menyuarakan /merubah bunyi نْ menjadi suara mim (مْ), dengan merapatkan dua bibir serta mendengung.</span></p>
  <p></p>
  <p><span style="font-weight: 400;">Contoh dari Hukum Iqlab : <img src = 'assets/foto/nun/iqlab/iqlab.png'><img src = 'assets/foto/nun/iqlab/iqlab1.png'></span></p>
  <p></p>
</div>
